#include "patrol.h"

Patrol::Patrol(std::string name, int init, std::vector<Monster> monsters){
  this->setName(name);
  this->setInit(init);
  this->setMembers(members);
}


std::string Patrol::getName() const{
  return this->name;
}

void Patrol::setName(std::string n){
  this->name = n;
}


int Patrol::getInit() const{
  return this->init;
}

void Patrol::setInit(int i){
  this->init = i;
}


const std::vector<Monster>* Patrol::getMembers() const{
  return &this->members;
}

void Patrol::setMembers(std::vector<Monster> m){
  this->members = m;
}