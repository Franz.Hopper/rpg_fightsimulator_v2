#include <QApplication>
#include "mainwindow.h"


#include <QDebug>
#include <vector>
#include "fight.h"

void isPlaying(Fight f){

  for(auto it = f.getEvents()->cbegin(); it != f.getEvents()->cend(); it++){
    if(it->getTime() == 0 && it->getInit() == f.getInit())
      qDebug() << QString::fromStdString(it->getNote());
  }

  for(int i=0; i < f.getNumberPlayers(); i++){
      qDebug() << QString::fromStdString((f.getNextPlayer()-i-1)->getName());
  }
  qDebug() << "";

  for(int i=0; i < f.getNumberPatrols(); i++){
      qDebug() << QString::fromStdString((f.getNextPatrol()-i-1)->getName());
  }
  qDebug() << "";
}

int main(int argc, char *argv[])
{
//    QApplication a(argc, argv);
//    MainWindow w;
//    w.show();
//    return a.exec();


    //////////////////////////////////////////////////////
      /// Testing of kernel functions ///
   //////////////////////////////////////////////////////

    std::vector<Player> p;
    std::vector<Status> status;
    Player player1("Joueur 1", State::unconscious, 1, status);
    Player player2("Joueur 2", State::unconscious, 3, status);
    Player player3("Joueur 3", State::unconscious, 5, status);
    p.push_back(player1);
    p.push_back(player3);
    p.push_back(player2);

    std::vector<Patrol> m;
    std::vector<Monster> monsters;
    Patrol patrol1("Patrouille 1", 1, monsters);
    Patrol patrol2("Patrouille 2", 2, monsters);
    Patrol patrol3("Patrouille 3", 3, monsters);
    m.push_back(patrol1);
    m.push_back(patrol2);
    m.push_back(patrol3);

    std::vector<Event> e;
    Event event0("Le Silmarilion", 0, 0,"Une cosmologie d'enfer.");
    Event event1("Le hobbit", 0, 2,"Un prequel pas si terrible.");
    Event event2("La communauté de l'anneau", 0, 1,"Le début d'une super histoire.");
    Event event3("Les deux tours", 1, 2,"Très certainement le meilleur des trois.");
    Event event4("Le retour du roi", 1, 1,"Pour finir en beauté.");
    e.push_back(event3);
    e.push_back(event1);
    e.push_back(event4);
    e.push_back(event2);
    e.push_back(event0);

    qDebug() << "Au commencement :";
    qDebug() << "player vector : ";
    for(unsigned int i=0; i<p.size(); i++){
        qDebug() << QString::fromStdString(p.at(i).getName());
    }
    qDebug() << "";
    qDebug() << "patrol vector : ";
    for(unsigned int i=0; i<m.size(); i++){
        qDebug() << QString::fromStdString(m.at(i).getName());
    }
    qDebug() << "";
    qDebug() << "event vector : ";
    for(unsigned int i=0; i<e.size(); i++){
        qDebug() << QString::fromStdString(e.at(i).getName());
    }
    qDebug() << "";

    qDebug() << "Tri :";
    std::sort (p.begin(), p.end(), greater());
    std::sort (m.begin(), m.end(), greater());
    std::sort (e.begin(), e.end());

    qDebug() << "player vector : ";
    for(unsigned int i=0; i<p.size(); i++){
        qDebug() << QString::fromStdString(p.at(i).getName());
    }
    qDebug() << "";
    qDebug() << "patrol vector : ";
    for(unsigned int i=0; i<m.size(); i++){
        qDebug() << QString::fromStdString(m.at(i).getName());
    }
    qDebug() << "";
    qDebug() << "event vector : ";
    for(unsigned int i=0; i<e.size(); i++){
        qDebug() << QString::fromStdString(e.at(i).getName());
    }
    qDebug() << "";

    Fight f(p, m, e);

    for(int i=0; i<15; i++){
      isPlaying(f);
      f.nextTurn();
      qDebug() << "Nouveau tour";
    }

    return 0;
}

