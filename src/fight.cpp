#include <algorithm>
#include "fight.h"


Fight::Fight(std::vector<Player> p,std::vector<Patrol> m, std::vector<Event> e){
  this->setPlayers(p);
  this->setNextPlayer(this->getPlayers()->cbegin());
  this->setNumberPlayers(0);
  this->setPatrols(m);
  this->setNextPatrol(this->getPatrols()->cbegin());
  this->setNumberPatrols(0);
  this->setEvents(e);
  this->setRound(1);
  this->setInit(this->getHighestInit());
}


int Fight::getRound() const{
  return this->round;
}

void Fight::setRound(int r){
  this->round = r;
}


int Fight::getInit() const{
  return this->init;
}

void Fight::setInit(int i){
  this->init = i;
}


const std::vector<Player>* Fight::getPlayers() const{
  return &this->players;
}

void Fight::setPlayers(std::vector<Player> p){
  this->players = p;
}


const std::vector<Player>::const_iterator Fight::getNextPlayer() const{
  return this->nextPlayer;
}

void Fight::setNextPlayer(std::vector<Player>::const_iterator it){
  this->nextPlayer = it;
}


int Fight::getNumberPlayers() const{
  return this->numberPlayers;
}

void Fight::setNumberPlayers(int np){
  this->numberPlayers = np;
}


const std::vector<Patrol>* Fight::getPatrols() const{
  return &this->patrols;
}

void Fight::setPatrols(std::vector<Patrol> p){
  this->patrols = p;
}


const std::vector<Patrol>::const_iterator Fight::getNextPatrol() const{
  return this->nextPatrol;
}

void Fight::setNextPatrol(std::vector<Patrol>::const_iterator it){
  this->nextPatrol = it;
}


int Fight::getNumberPatrols() const{
  return this->numberPatrols;
}

void Fight::setNumberPatrols(int np){
  this->numberPatrols = np;
}


const std::vector<Event>* Fight::getEvents() const{
  return &this->events;
}

void Fight::setEvents(std::vector<Event> e){
  this->events = e;
}


int Fight::nextTurn(){

  // Check the existence of the next player and the next patrol
  int initPlayer = (nextPlayer == players.cend())? 0 : nextPlayer->getInit();
  int initPatrol = (nextPatrol == patrols.cend())? 0 : nextPatrol->getInit();

  // Reset current playing players and patrols
  setNumberPatrols(0);
  setNumberPlayers(0);

  // "init" is max of players, patrols and events
  this->setInit(std::max(initPlayer, initPatrol));
  this->setInit(std::max(this->getInit(), this->getEvents()->at(0).getInit()));

  // Special case : No players and patrols left
  if(this->getInit() == 0){
    this->setInit(this->getHighestInit());
    this->newRound();
    return 0;
  }

  // Update number of players and patrols who are playing
  while(this->getNextPlayer() != this->getPlayers()->cend()
     && this->getInit() == this->getNextPlayer()->getInit()){
    this->setNumberPlayers(this->getNumberPlayers()+1);
    this->setNextPlayer(this->getNextPlayer()+1);
  }

  while(this->getNextPatrol() != this->getPatrols()->cend()
     && this->getInit() == this->getNextPatrol()->getInit()){
    this->setNumberPatrols(this->getNumberPatrols()+1);
    this->setNextPatrol(this->getNextPatrol()+1);
  }

  return this->getInit();
}


int Fight::newRound(){
  this->setRound(this->getRound()+1);
  this->setNextPlayer(this->getPlayers()->cbegin());
  this->setNextPatrol(this->getPatrols()->cbegin());

  // auto stand here for std::vector<Player>::iterator
  for(auto it = this->events.begin(); it != this->events.end(); it++){
    it->setTime(it->getTime()-1);
  }
  return this->getRound();
}


int Fight::getHighestInit() const{
  int maxInit = -1;

  if(this->getPlayers()->size() != 0)
    maxInit = this->getPlayers()->at(0).getInit();

  if(this->getPatrols()->size() != 0)
    maxInit = std::max(maxInit,this->getPatrols()->at(0).getInit());

  int i=0;
  while(i < this->getEvents()->size() && this->getEvents()->at(i).getTime()==0
     && this->getEvents()->at(i).getInit() == 0){
      i++;
  }
  if (i < this->getEvents()->size() && this->getEvents()->at(i).getTime()==0)
    maxInit = std::max(maxInit,this->getEvents()->at(i).getInit());

  return maxInit;
}
