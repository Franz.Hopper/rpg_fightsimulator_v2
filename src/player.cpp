#include "player.h"

Player::Player(std::string name, enum State state, int init, std::vector<Status> status){
  this->setName(name);
  this->setState(state);
  this->setInit(init);
  this->setStatus(status);
}

std::string Player::getName() const{
  return this->name;
}

void Player::setName(std::string n){
  this->name = n;
}

enum State Player::getState() const{
  return this->state;
}

void Player::setState(enum State s){
  this->state = s;
}

int Player::getInit() const{
  return this->init;
}

void Player::setInit(int i){
  this->init = i;
}

const std::vector<Status> Player::getStatus() const{
  return this->status;
}

void Player::setStatus(std::vector<Status> s){
  this->status = s;
}