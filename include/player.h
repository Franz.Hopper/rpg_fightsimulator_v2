#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <vector>
#include "status.h"


/*! \enum State
 * \brief Player state.
*/
enum State{
  fight,
  unconscious,
  delay,
  ready
};


/*! \class Player
 * \brief A player in a fight
 *
 * This class represent a player
 */

class Player {

  //Attributes
  protected:
    std::string name;
    enum State state;
    int init;
    std::vector<Status> status;


  //Methods
  public:

    /*!
     * \brief Player class contructor.
     *
     * \param name name of the player.
     * \param state state of the player.
     * \param init init of the player.
     * \param status status of the player.
    */
    Player(std::string name, enum State state, int init, std::vector<Status> status);

    /*!
     * \brief Getter of attribute name.
     *
     * \return The name of the player.
    */
    std::string getName() const;


    /*!
     * \brief Setter of attribute name.
     *
     * \param n The name of the player.
    */
    void setName(std::string n);

    /*!
     * \brief Getter of attribute state.
     *
     * \return The state of the player.
    */
    enum State getState() const;


    /*!
     * \brief Setter of attribute state.
     *
     * \param s The state of the player.
    */
    void setState(enum State s);

    /*!
     * \brief Getter of attribute init.
     *
     * \return The init of the player.
    */
    int getInit() const;


    /*!
     * \brief Setter of attribute init.
     *
     * \param i The init of the player.
    */
    void setInit(int i);

    /*!
     * \brief Getter of attribute status.
     *
     * \return The status of the player.
    */
    const std::vector<Status> getStatus() const;


    /*!
     * \brief Setter of attribute Status.
     *
     * \param s The status of the player.
    */
    void setStatus(std::vector<Status> s);


};


// Operator

// Player are compared according to their initiative.
inline bool operator< (const Player& lhs, const Player& rhs){
  return lhs.getInit() < rhs.getInit();
}

inline bool operator> (const Player& lhs, const Player& rhs){
  return rhs < lhs;
}


#endif // PLAYER_H