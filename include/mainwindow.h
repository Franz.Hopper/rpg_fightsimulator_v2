#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>


class MainWindow : public QWidget
{
  Q_OBJECT

  // Attributes
  
  // Methods
  public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
};

#endif // MAINWINDOW_H
