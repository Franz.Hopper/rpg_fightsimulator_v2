#ifndef STATUS_H
#define STATUS_H

#include <string>

/*! \class Status
 * \brief status of a player or monster, alert the user.
 *
 * This class represent some player's status or monster's status.
 */

class Status {

  // Attributes
  protected:
    std::string name;
    int duration;
    std::string note;
  
  // Methods
  public:

    /*!
     * \brief Status class contructor.
     *
     * \param name Name of the status.
     * \param duration duration of the status.
     * \param note message to send to the user.
    */
    Status(std::string name, int duration, std::string note);


    /*!
     * \brief Getter of attribute name.
     *
     * \return The name of the status.
    */
    std::string getName() const;


    /*!
     * \brief Setter of attribute name.
     *
     * \param n The name of the status.
    */
    void setName(std::string n);


    /*!
     * \brief Getter of attribute duration.
     *
     * \return The duration of the status.
    */
    int getDuration() const;


    /*!
     * \brief Setter of attribute duration.
     *
     * \param d The duration of the status.
    */
    void setDuration(int d);


    /*!
     * \brief Getter of attribute note.
     *
     * \return The message to send to user.
    */
    std::string getNote() const;


    /*!
     * \brief Setter of attribute note.
     *
     * \param n A string og the message to send to the user.
    */
    void setNote(std::string n);
};

#endif // STATUS_H