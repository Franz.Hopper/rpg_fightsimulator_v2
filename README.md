# RPG_FightSimulator_v2

- [RPG_FightSimulator_v2](#rpg_fightsimulator_v2)
  - [Desciption](#desciption)
  - [Screenshots](#screenshots)
  - [Installation and prerequisites](#installation-and-prerequisites)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
    - [User manual](#user-manual)
  - [Licence](#licence)
  - [Authors](#authors)

## Desciption  

RPG_FightSimulator_v2 is a small tool which helps you to manage your fights
in your RPG campaigns


## Screenshots  

## Installation and prerequisites

### Prerequisites  

### Installation  

### User manual  

## Licence

This file is part of RPG_FightSimullator_v2.

RPG_FightSimullator_v2 is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

RPG_FightSimullator_v2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You could find a copy of the GNU General Public License in [COPYING](COPYING).
If not, see <https://www.gnu.org/licenses/>.


## Authors  

